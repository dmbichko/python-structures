"""
Given a string, you need to reverse the order of characters in each word
within a sentence while still preserving whitespace and initial word order.

In the string, each word is separated by single space
and there will not be any extra space in the string.
"""


def reverse_function(str):
    return str[::-1]


def main():
    """Perform list commands."""
    string = input()
    revstr_list = []
    # splitting the string on space
    words = string.split()
    for i in words:
        rev_str = reverse_function(i)
        revstr_list.append(rev_str)
    # joining the words and printing
    print(" ".join(revstr_list))


if __name__ == "__main__":
    main()
