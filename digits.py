"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = int(input())
    value = 0
    while (n != 0):
        value = value + n % 10
        n = n // 10
    print(value)


if __name__ == "__main__":
    main()
