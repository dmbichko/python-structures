from mock import patch
import pytest

import split_join


@pytest.mark.parametrize("num, expected", [
    ("Let's take LeetCode contest", "s'teL ekat edoCteeL tsetnoc"),
    ("", ""),
    ("2", "2"),
    ("ab ba", "ba ab"),
])
@patch('builtins.input')
def test_main(input_mock, num, expected):
    input_mock.return_value = num
    with patch('builtins.print') as print_mock:
        split_join.main()
        print_mock.assert_called_with(expected)
