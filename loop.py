"""
!Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""


def main():
    """Factorial calculation."""
    count = 1
    n = int(input())
    while n > 0:
        count = count * n
        n -= 1
    print(count)


if __name__ == "__main__":
    main()
