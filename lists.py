"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each command
in order and perform the corresponding operation on your list.
The first line contains an integer, denoting the number of commands.
Each line  of the  subsequent lines contains one of the commands described above.
"""


def main():
    """Perform list commands."""
    n = int(input())
    list = []
    lst = []
    sub_str = []
    for i in range(0, n):
        incom = input()
        lst.append(incom)
    for i in range(0, n):
        command = str(lst[i])
        if command.find('insert') != -1:
            sub_str = command.split()
            list.insert(int(sub_str[1]), int(sub_str[2]))
        elif command.find('print') != -1:
            print(list)
        elif command.find('remove') != -1:
            sub_str = command.split()
            list.remove(int(sub_str[1]))
        elif command.find('append') != -1:
            sub_str = command.split()
            list.append(int(sub_str[1]))
        elif command.find('sort') != -1:
            list.sort()
        elif command.find('pop') != -1:
            list.pop()
        elif command.find('reverse') != -1:
            list.reverse()


if __name__ == "__main__":
    main()
